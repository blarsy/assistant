import React from 'react'
import PropTypes from 'prop-types'
import { map, filter, sort } from 'ramda'
import { GuardedButton, Collapsible, FixedCols, TipZone } from './ui/'
import agent from './agent/agent'

const Holdings = props => {
  const { data } = props
  const pairNames = Object.keys(data.pairs)
  const holdings = sort((a,b) => {
    if(a.btcAmount > b.btcAmount) return -1
    if(a.btcAmount == b.btcAmount) return 0
    return 1
  }, map(
    pairName => data.pairs[pairName],
    filter(pairName => data.pairs[pairName].btcAmount, pairNames)
  ))

  return (
    <article>
      <h2>Holdings</h2>
      <p><em>Total value (BTC)</em> {data.totalValue}</p>
      <FixedCols cols="3">
        <span>Currency</span>
        <span>Evolution</span>
        <span>Action</span>
      </FixedCols>
      {holdings.map(currency => {
        const mostRelevantPrice = currency.bestSell
          ? currency.bestSell.realizedPrice
          : currency.price
        const mostRelevantAmount = currency.bestSell
          ? currency.bestSell.btcAmount
          : currency.btcAmount
        const evolution = mostRelevantPrice / currency.acquiredPrice

        if (currency.currency === 'BTC') {
          return (
            <FixedCols cols="3" key={currency.currency}>
              <span>{currency.currency}</span>
              <span>
                {`${currency.btcAmount.toFixed(4)}`}
              </span>
              <span>
                ({((currency.price / currency.acquiredPrice - 1) * 100).toFixed(2)} % from last
                buy)
              </span>
            </FixedCols>
          )
        }
        return (
          <FixedCols key={currency.currency} cols="3">
            <span>
              {currency.currency}
            </span>
            <div>
              <Collapsible
                summary={`${mostRelevantAmount.toFixed(4)} (${((evolution - 1) * 100).toFixed(
                  2
                )} %)`}
              >
                <TipZone>
                  <p>
                    {currency.bestSell
                      ? 'Price is best sell according to the latest known state of the order book'
                      : 'Current market price. Actual realized price may differ'}
                  </p>
                  <ul>
                    <li>Market price: {currency.price.toFixed(8)}</li>
                    <li>
                      Best sell price:{' '}
                      {currency.bestSell ? currency.bestSell.realizedPrice.toFixed(8) : 'Unknown'}
                    </li>
                    <li>
                      Last buy price: {currency.acquiredPrice && currency.acquiredPrice.toFixed(8)}
                    </li>
                  </ul>
                </TipZone>
              </Collapsible>
            </div>
            <span>
              {evolution < 1 &&
                <GuardedButton
                  label="Sell at loss"
                  name={`sell${currency.currency}`}
                  action={() => agent.sellImmediately(currency)}
                />}
              {evolution > 1 &&
                <GuardedButton
                  label="Reap winst"
                  name={`sell${currency.currency}`}
                  action={() => agent.sellImmediately(currency)}
                />}
              {currency.openOrders &&
                currency.openOrders.length > 0 &&
                <span>{currency.openOrders.length} opened order(s) would be cancelled</span>}
            </span>
          </FixedCols>
        )
      })}
    </article>
  )
}

Holdings.propTypes = {
  data: PropTypes.object.isRequired
}

export default Holdings
