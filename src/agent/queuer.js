import EventEmitter from 'events'

class Queuer extends EventEmitter {
  constructor(interval) {
    super()
    this.tasks = []

    this.tick = this.tick.bind(this)
    this.execute = this.execute.bind(this)

    this.setMaxListeners(100)

    this.interval = setInterval(this.tick, interval)
  }

  execute(task) {
    this.tasks.push(task)
    const emitter = this

    return new Promise(resolve => {
      const listener = function (result, activeTask) {
        if (activeTask === task) {
          resolve(result)
          emitter.off('TaskDone', listener)
        }
      }
      this.on('TaskDone', listener)
    })
  }

  tick() {
    if (this.tasks.length > 0) {
      const activeTask = this.tasks.shift()
      activeTask().then(result => this.emit('TaskDone', result, activeTask))
    }
  }
}

const queuer = new Queuer(1000)
export default queuer
