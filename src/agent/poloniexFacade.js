import { get } from 'axios'
import { createWriteStream } from 'fs'
import { contains, forEach } from 'ramda'
import Poloniex from 'poloniex-api-node'
import EventEmitter from 'events'
import util from 'util'
import settings from './settings.json'

class PoloniexFacade extends EventEmitter {
  constructor(watchedPairs, appId, secret) {
    super()
    this.watchedPairs = watchedPairs
    this.initializing = false
    this.poloniex = new Poloniex(appId, secret)
    this.logStream = null

    this.init = this.init.bind(this)
    this.log = this.log.bind(this)

    this.initializer = new Promise(resolve => this.init().then(resolve))
  }

  startToEmitOrderbooksUpdates() {
    forEach(pair => this.poloniex.subscribe(pair), this.watchedPairs)

    this.poloniex.on('open', () => {
      this.log('Poloniex WebSocket connection open')
    })

    this.poloniex.on('close', () => {
      this.log('Poloniex WebSocket connection disconnected')
    })

    this.poloniex.on('error', error => {
      this.log(`An error has occured: ${JSON.stringify(error)}`)
    })

    this.poloniex.on('message', this.messageReceived.bind(this))
    this.poloniex.openWebSocket()
  }

  async init() {
    this.logStream = createWriteStream('./log.txt')
    await new Promise(resolve => this.logStream.write(`${new Date()}\n`, resolve))
    // return promisify(writeFile)('./log.txt', `${new Date()}\n`)
  }

  async log(msg, data) {
    return this.initializer.then(
      () =>
        new Promise(resolve =>
          this.logStream.write(`${new Date()} - ${msg}\n${util.format('%j', data)}\n`, resolve)
        )
    )
  }

  async logAndReturn(methodName, res) {
    try {
      await this.log(`Method ${methodName}:`, res)
    } catch (ex) {}
    return res
  }

  async messageReceived(channelName, data, seq) {
    try {
      if (contains(channelName, this.watchedPairs)) {
        this.emit('orderBookModify', channelName, data, seq)
      }
    } finally {
      await this.log('Websocket message received', { channelName, seq, data })
    }
  }

  async returnCompleteBalances() {
    return this.logAndReturn('returnCompleteBalances', await this.poloniex.returnCompleteBalances())
  }
  async returnOpenOrders() {
    return this.logAndReturn('returnOpenOrders', await this.poloniex.returnOpenOrders('all'))
  }
  async returnMyTradeHistory(...args) {
    return this.logAndReturn(
      'returnMyTradeHistory',
      await this.poloniex.returnMyTradeHistory(...args)
    )
  }

  async callMethod(method) {
    const res = await get(method)
    return this.logAndReturn(method, res)
  }

  async returnTicker() {
    return this.logAndReturn('returnTicker', await this.poloniex.returnTicker())
  }

  async returnChartData(...args) {
    return this.logAndReturn('returnChartData', await this.poloniex.returnChartData(...args))
  }

  async sell(...args) {
    return this.logAndReturn('sell', await this.poloniex.sell(...args))
  }

  async buy(...args) {
    return this.logAndReturn('buy', await this.poloniex.buy(...args))
  }

  async cancelOrder(...args) {
    return this.logAndReturn('cancelOrder', await this.poloniex.cancelOrder(...args))
  }
}

export default new PoloniexFacade(
  settings.watchedPairs,
  settings.poloniex.appId,
  settings.poloniex.secret
)
