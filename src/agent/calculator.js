import { map, sort, filter, forEach } from 'ramda'
import queuer from './queuer'
import poloniex from './poloniexFacade'

const getBestSellInfo = (orderbook, amount) => {
  let remaining = amount
  let total = 0
  let bidIndex = 0
  const bidEntries = sort((a, b) => {
    const aNum = +a[0]
    const bNum = +b[0]
    if (aNum > bNum) {
      return -1
    } else if (aNum === bNum) {
      return 0
    }
    return 1
  }, Object.entries(orderbook.bids))
  while (remaining > 0) {
    const bid = bidEntries[bidIndex]
    if (+bid[1] < remaining) {
      total += +bid[0] * +bid[1]
      remaining -= +bid[1]
    } else {
      total += +bid[0] * remaining
      remaining = 0
    }
    bidIndex += 1
  }
  return {
    btcAmount: total,
    realizedPrice: total / amount
  }
}

const getBestBuyInfo = (orderbook, btcAmounts) =>
  map(btcAmount => {
    let remaining = btcAmount
    let askIndex = 0
    let totalBought = 0
    const askEntries = Object.entries(orderbook.asks)
    while (remaining > 0) {
      const ask = askEntries[askIndex]
      if (+ask[1] < remaining) {
        totalBought += +ask[1] / +ask[0]
        remaining -= +ask[1]
      } else {
        totalBought += remaining / +ask[0]
        remaining = 0
      }
      askIndex += 1
    }
    return {
      btcAmount,
      amount: totalBought,
      realizedPrice: btcAmount / totalBought
    }
  }, btcAmounts)

export const getLowsAndHighs = watchedPairs => {
  const promises = []

  for (let i = 0; i < watchedPairs.length; i += 1) {
    const pair = watchedPairs[i]

    promises.push(
      queuer.execute(() => {
        const callTime = Math.abs(Number(new Date()) / 1000)
        return poloniex
          .returnChartData(pair, 86400, callTime - 90 * 24 * 60 * 60)
          .then(priceHistory => {
            let high1Month = 0
            let high3Month = 0
            let low1Month = Infinity
            let low3Month = Infinity
            const prices1Month = filter(
              dayInfo => +dayInfo.date >= Math.abs(callTime - 30 * 24 * 60 * 60),
              priceHistory
            )
            const prices3Month = filter(
              dayInfo => +dayInfo.date >= Math.abs(callTime - 90 * 24 * 60 * 60),
              priceHistory
            )
            forEach(dayInfo => {
              high1Month = dayInfo.high > high1Month ? dayInfo.high : high1Month
            }, prices1Month)
            forEach(dayInfo => {
              high3Month = dayInfo.high > high3Month ? dayInfo.high : high3Month
            }, prices3Month)
            forEach(dayInfo => {
              low1Month = dayInfo.low < low1Month ? dayInfo.low : low1Month
            }, prices1Month)
            forEach(dayInfo => {
              low3Month = dayInfo.low < low3Month ? dayInfo.low : low3Month
            }, prices3Month)
            return { pair, high1Month, high3Month, low1Month, low3Month }
          })
      })
    )
  }

  return Promise.all(promises)
}

export const calculateBestPricesFromOrderbook = (orderbookChangeInfo, data) => {
  if (!data) return

  const targetPair = data.pairs[orderbookChangeInfo.pair]
  if (targetPair) {
    if (targetPair.btcAmount) {
      targetPair.bestSell = getBestSellInfo(orderbookChangeInfo.orderbook, targetPair.amount)
    }
    targetPair.bestBuy = getBestBuyInfo(orderbookChangeInfo.orderbook, [0.3, 0.4, 0.5])
  }

  // refresh total value
  const pairs = Object.keys(data.pairs)
  const dataToModify = data
  let totalValue = 0
  for (let i = 0; i < pairs.length; i += 1) {
    const currentPair = data.pairs[pairs[i]]
    let currentBtcAmount = 0
    if (currentPair.currency === 'BTC') {
      currentBtcAmount = currentPair.amount
    } else if (currentPair.bestSell) {
      currentBtcAmount = currentPair.bestSell.btcAmount
    } else if (currentPair.btcAmount) {
      currentBtcAmount = currentPair.btcAmount
    }
    // console.log(`Was ${totalValue.toFixed(8)}, adding ${currentBtcAmount.toFixed(8)}`)
    totalValue += currentBtcAmount
  }
  dataToModify.totalValue = totalValue.toFixed(8)
}
