// Deprecated

import { forEach, filter, map, takeWhile, reduce, contains, find } from 'ramda'
import { fromUnixTimeStamp, toTime } from './display'
import { getSimpleUpSlopes, getSlopes } from './slope'
import PoloniexFacade from './poloniexFacade'

const watchedPairs = [
  'BTC_ETC',
  'BTC_ZEC',
  'BTC_DCR',
  'BTC_BCHSV',
  'BTC_FCT',
  'BTC_BCHABC',
  'BTC_XEM',
  'BTC_BAT',
  'BTC_REP',
  'BTC_LTC',
  'BTC_XRP',
  'BTC_STR',
  'BTC_EOS',
  'BTC_XMR',
  'BTC_BNT',
  'BTC_ETH',
  'BTC_OMG',
  'BTC_DASH',
  'BTC_GNT',
  'USDT_BTC'
]
const poloniex = new PoloniexFacade(
  watchedPairs,
  'NIBX0M28-GZI8NE7V-LQFHKDQJ-0GVIWCO9',
  '267d1e48941c94c478c8eccba35aba7f5416dd9f88f171fe55c4cb17107b4f7b7064a915ecb09e7333565fd51d1202bb306f9d832e79035c5bce1996920cecf4'
)

async function printSimpleUpSlopes() {
  const priceHistory = await poloniex.callMethod(
    'https://poloniex.com/public?command=returnChartData&currencyPair=BTC_BCH&start=1515101400&end=9999999999&period=1800'
  )
  forEach(
    upSlope =>
      console.log(
        `Start: ${fromUnixTimeStamp(upSlope.firstQuote.date)}, end: ${fromUnixTimeStamp(
          upSlope.lastQuote.date
        )}, low: ${upSlope.firstQuote.weightedAverage}, high: ${upSlope.lastQuote
          .weightedAverage}, duration: ${toTime(
          upSlope.lastQuote.date - upSlope.firstQuote.date
        )}, percentage: ${upSlope.percent}, steepness: ${upSlope.steep}`
      ),
    filter(slope => slope.percent > 5, getSimpleUpSlopes(priceHistory.data))
  )
}

async function printSlopes() {
  const priceHistory = await poloniex.callMethod(
    'https://poloniex.com/public?command=returnChartData&currencyPair=BTC_BCH&start=1515101400&end=9999999999&period=1800'
  )
  forEach(
    upSlope =>
      console.log(
        `Start: ${fromUnixTimeStamp(upSlope.firstQuote.date)}, end: ${fromUnixTimeStamp(
          upSlope.lastQuote.date
        )}, low: ${upSlope.firstQuote.weightedAverage}, high: ${upSlope.lastQuote
          .weightedAverage}, duration: ${toTime(
          upSlope.lastQuote.date - upSlope.firstQuote.date
        )}, percentage: ${upSlope.percent}, steepness: ${upSlope.steep}`
      ),
    getSlopes(priceHistory.data, steepness => steepness < -1)
  )
}

async function getBalances() {
  const balances = await poloniex.returnCompleteBalances()
  return map(
    key => ({ curr: key, value: balances[key].btcValue }),
    filter(key => +balances[key].btcValue > 0.0001, Object.keys(balances))
  )
}

async function getPosition(curr) {
  const pair = `BTC_${curr}`

  const trades = await poloniex.returnMyTradeHistory(
    pair,
    Math.round(Number(new Date() / 1000) - 60 * 60 * 24 * 90),
    Math.round(Number(new Date()) / 1000)
  )
  const acc = (current, trade) => {
    current.total += Number(trade.total)
    current.amount += Number(trade.amount)
    return current
  }
  if (trades.length > 0) {
    const accumulator = { total: 0, amount: 0 }
    let totals
    let type
    if (trades[0].type === 'buy') {
      type = 'buy'
      const lastBuys = takeWhile(trade => trade.type === 'buy', trades)
      totals = reduce(acc, accumulator, lastBuys)
    } else {
      type = 'sell'
      const lastSales = takeWhile(trade => trade.type === 'sell', trades)
      totals = reduce(acc, accumulator, lastSales)
    }
    return {
      type,
      lastPrice: totals.total / totals.amount,
      lastQuantity: totals.total
    }
  }
  return null
}

async function getHistory() {
  const result = []
  const trades = await poloniex.returnMyTradeHistory(
    'all',
    Math.round(Number(new Date() / 1000) - 60 * 60 * 24 * 90),
    Math.round(Number(new Date()) / 1000)
  )
  const acc = (current, trade) => {
    current.total += Number(trade.total)
    current.amount += Number(trade.amount)
    return current
  }
  for (const pair in trades) {
    const pairTrades = trades[pair]
    if (pairTrades.length > 0) {
      const accumulator = { total: 0, amount: 0 }
      const history = { pair }
      let totals
      if (pairTrades[0].type === 'buy') {
        history.type = 'buy'
        const lastBuys = takeWhile(trade => trade.type === 'buy', pairTrades)
        totals = reduce(acc, accumulator, lastBuys)
      } else {
        history.type = 'sell'
        const lastSales = takeWhile(trade => trade.type === 'sell', pairTrades)
        totals = reduce(acc, accumulator, lastSales)
      }

      history.lastPrice = totals.total / totals.amount
      history.lastQuantity = totals.total
      result.push(history)
    } else {
      result.push({
        lastQuantity: 0
      })
    }
  }
  return result
}

export async function getWatchedCurrencies() {
  const result = {
    pairs: []
  }
  const balances = await getBalances()
  const history = await getHistory()
  const prices = await poloniex.returnTicker()

  for (let i = 0; i < watchedPairs.length; i += 1) {
    const pair = watchedPairs[i]
    const currency = pair.substring(pair.indexOf('_') + 1)
    const lastHistory = find(historyItem => historyItem.pair === pair, history)
    const balance = find(b => b.curr === currency, balances)
    const current = { currency, price: +prices[pair].last }
    if (balance) {
      current.btcAmount = +balance.value
      current.amount = +balance.available + +balance.onOrders
      if (
        lastHistory &&
        (lastHistory.type === 'buy' || currency === 'USDT' && lastHistory.type === 'sell')
      ) {
        current.acquiredPrice = lastHistory.lastPrice
      }
    }
    if (
      lastHistory &&
      (lastHistory.type === 'sell' || currency === 'USDT' && lastHistory.type === 'buy')
    ) {
      current.lastSalePrice = lastHistory.lastPrice
    }
    result.pairs.push(current)
  }

  result.totalValue = reduce(
    (balance, currency) => balance + (currency.btcAmount || 0),
    0,
    result.pairs
  )

  return result
}

export async function getOpenOrders() {
  const openOrders = await poloniex.returnOpenOrders()

  return openOrders
}

export async function getOrderbook(currencyPair) {
  const orderbook = await poloniex.getOrderbook(currencyPair)

  return orderbook
}

async function getSuggestedBuys() {
  // get order books of all followed-up currencies
  // Calculate some numbers that give an clue of the interesting facts about the order books
}

async function getSuggestedSales(currencies) {
  const result = {}
  const prices = await poloniex.returnTicker()

  // Calculate some numbers that give an clue of the interesting facts about the order books
  const usefulCurrencies = map(c => `BTC_${c}`, currencies)
  for (let i = 0; i < watchedPairs.length; i++) {
    const pair = watchedPairs[i]
    console.log(pair)
    if (contains(pair, usefulCurrencies)) {
      const price = prices.data[pair].last
      const priceGradient = map(p => price * p, [
        0.95,
        0.96,
        0.97,
        0.98,
        0.99,
        1,
        1.01,
        1.02,
        1.03,
        1.04,
        1.05
      ])
      const book = await poloniex.getOrderbook(pair)

      const bookVolumes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      const shouldBuy = []
      const shouldSell = []
      for (const bookPrice in book.bids) {
        for (let j = 0; j < 5; j++) {
          if (+bookPrice >= priceGradient[j] && +bookPrice < priceGradient[j + 1]) {
            bookVolumes[j] += +book.bids[bookPrice]
            break
          }
        }
      }
      for (const bookPrice in book.asks) {
        for (let j = 5; j < 10; j++) {
          if (+bookPrice >= priceGradient[j] && +bookPrice < priceGradient[j + 1]) {
            bookVolumes[j] += +book.asks[bookPrice]
            break
          }
        }
      }
      let cumulatedBids = 0
      let cumulatedAsks = 0
      for (let j = 0; j < 5; j++) {
        shouldBuy[i] = (bookVolumes[4 - j] + cumulatedBids) / (bookVolumes[5 + j] + cumulatedAsks)
        cumulatedBids += bookVolumes[4 - j]
        shouldSell[i] = (bookVolumes[5 + j] + cumulatedAsks) / (bookVolumes[4 - j] + cumulatedBids)
        cumulatedAsks += bookVolumes[5 + j]
      }
      console.log(`bookVolumes ${bookVolumes}`)
      result[pair] = { shouldBuy, shouldSell }
    }
  }
  return result
}

async function displaySuggestedBuysAndSells() {
  const balances = await getBalances()
  // Create list of currency pairs that look like in a good situation to buy
  // const suggestedBuys = await getSuggestedBuys()
  // Create list of currencies that we may want to sell
  const suggestedSales = await getSuggestedSales(
    map(balance => balance.curr, filter(b => b.curr !== 'BTC', balances))
  )

  console.log(suggestedSales)
}

// displayPositions().then(() => process.exit())
// displaySuggestedBuysAndSells().then(() => process.exit())
// process.exit()
