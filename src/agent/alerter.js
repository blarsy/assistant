import { forEach, find } from 'ramda'
import agent from './agent'

const notifyAlert = (data, alerts, slug, msg, actions) => {
  if (data.alerts) {
    const existingAlert = find(alert => alert.slug === slug, data.alerts)
    if (existingAlert) {
      alerts.push(existingAlert)
    } else {
      alerts.push({ slug, msg, actions, spawnedAt: new Date() })
      new Notification(msg, { tag: slug, renotify: true })
    }
  } else {
    alerts.push({ slug, msg, actions })
    new Notification(msg, { tag: slug, renotify: true })
  }
}

const formatPercentDiff = (price1, price2) => {
  const ratio = price1 / price2
  const percent = (ratio - 1) * 100
  return percent.toFixed(2)
}

export const getAlerts = data => {
  const alerts = []
  forEach(currency => {
    const buyAction = {
      name: 'Buy 0.3BTC',
      action: async () => agent.buyImmediately(currency, 0.3)
    }
    const sellAction = {
      name: 'Sell Immediately',
      action: async () => agent.sellImmediately(currency)
    }
    if (currency.acquiredPrice) {
      const mostRelevantPrice = currency.bestSell ? currency.bestSell.realizedPrice : currency.price
      if (mostRelevantPrice / currency.acquiredPrice < 0.95) {
        notifyAlert(
          data,
          alerts,
          `${currency.currency}holdingdropped`,
          `${currency.currency} holding dropped by ${(Math.abs(
            mostRelevantPrice / currency.acquiredPrice - 1
          ) * 100).toFixed(2)}%`,
          [sellAction]
        )
      }
    }
    if (currency.pastPrice) {
      const priceChange = currency.price / currency.pastPrice.price
      if (priceChange < 0.99) {
        notifyAlert(
          data,
          alerts,
          `${currency.currency}dropping`,
          `${currency.currency} is dropping by ${((1 - priceChange) * 100).toFixed(2)}%`,
          currency.acquiredPrice ? [sellAction] : [buyAction]
        )
      }
      if (priceChange > 1.01) {
        notifyAlert(
          data,
          alerts,
          `${currency.currency}spiking`,
          `${currency.currency} is spiking by ${((priceChange - 1) * 100).toFixed(2)}%`,
          currency.acquiredPrice ? [sellAction] : [buyAction]
        )
      }
    }
    if (currency.priceHistory) {
      if (currency.price / currency.priceHistory.low1Month < 1.05) {
        notifyAlert(
          data,
          alerts,
          `${currency.currency}closeto1monthlow`,
          `${currency.currency} is close to its 1 month low (${formatPercentDiff(
            currency.price,
            currency.priceHistory.low1Month
          )}%)`,
          !currency.acquiredPrice && [buyAction]
        )
      }
      if (currency.price / currency.priceHistory.low3Month < 1.05) {
        notifyAlert(
          data,
          alerts,
          `${currency.currency}closeto3monthlow`,
          `${currency.currency} is close to its 3 month low (${formatPercentDiff(
            currency.price,
            currency.priceHistory.low3Month
          )}%)`,
          !currency.acquiredPrice && [buyAction]
        )
      }
      if (currency.price / currency.priceHistory.high1Month > 0.95) {
        notifyAlert(
          data,
          alerts,
          `${currency.currency}closeto1monthhigh`,
          `${currency.currency} is close to its 1 month high (${formatPercentDiff(
            currency.price,
            currency.priceHistory.high1Month
          )}%)`,
          currency.acquiredPrice && [sellAction]
        )
      }
      if (currency.price / currency.priceHistory.high3Month > 0.95) {
        notifyAlert(
          data,
          alerts,
          `${currency.currency}closeto3monthhigh`,
          `${currency.currency} is close to its 3 month high (${formatPercentDiff(
            currency.price,
            currency.priceHistory.high3Month
          )}%)`,
          currency.acquiredPrice && [sellAction]
        )
      }
    }
  }, Object.values(data.pairs))
  return alerts
}
