export function fromUnixTimeStamp(ts) {
  return new Date(ts * 1000)
}
export function toTime(seconds) {
  let second = seconds
  const hours = Math.floor(seconds / 3600)
  second -= hours * 3600
  const minutes = Math.floor(seconds / 60)
  second -= minutes * 60
  return `${hours}:${minutes}:${second}`
}
