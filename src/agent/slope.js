function getSteepness(price1, price2, date1, date2) {
  const percent = (price2 - price1) / (price1 * 100)
  const steepness = percent / ((date2 - date1) / 3600)
  //console.log(`input ${price1} ${price2} ${date1} ${date2} => ${steepness}`)
  //console.log(`input ${percent} ${(date2 - date1) / 3600} => ${steepness}`)
  //console.log(steepness)
  return steepness
}

function calculateSlope(slope) {
  const percent =
    (slope.lastQuote.weightedAverage - slope.firstQuote.weightedAverage) /
    (slope.firstQuote.weightedAverage * 100)
  return {
    firstQuote: slope.firstQuote,
    lastQuote: slope.lastQuote,
    percent,
    steep: getSteepness(
      slope.firstQuote.weightedAverage,
      slope.lastQuote.weightedAverage,
      slope.firstQuote.date,
      slope.lastQuote.date
    )
  }
}

export const getSimpleUpSlopes = quotes => {
  const result = []
  let index = 0
  let currentUpSlope = {}
  while (index < quotes.length - 1) {
    if (quotes[index + 1].weightedAverage >= quotes[index].weightedAverage) {
      if (!currentUpSlope.firstQuote) {
        currentUpSlope.firstQuote = quotes[index]
      }
      currentUpSlope.lastQuote = quotes[index + 1]
    } else if (currentUpSlope.firstQuote) {
      result.push(calculateSlope(currentUpSlope))
      currentUpSlope = {}
    }
    index++
  }
  return result
}

export const getSlopes = (quotes, steepnessPredicate) => {
  const result = []
  let index = 0
  let currentUpSlope = {}
  while (index < quotes.length - 1) {
    if (currentUpSlope.firstQuote) {
      const candidateSlope = calculateSlope({
        firstQuote: currentUpSlope.firstQuote,
        lastQuote: quotes[index + 1]
      })
      if (!steepnessPredicate(candidateSlope.steep)) {
        result.push(
          calculateSlope({
            firstQuote: currentUpSlope.firstQuote,
            lastQuote: quotes[index]
          })
        )
        currentUpSlope = {}
      }
    } else {
      const steepness = getSteepness(
        quotes[index].weightedAverage,
        quotes[index + 1].weightedAverage,
        quotes[index].date,
        quotes[index + 1].date
      )
      if (steepnessPredicate(steepness)) {
        currentUpSlope.firstQuote = quotes[index]
      }
    }

    index++
  }
  return result
}
