import styled from 'styled-components'

const TipZone = styled.div`
  font-size: 0.75rem;
  > p {
    margin: 0;
  }
  > ul {
    margin: 0;
    margin-bottom: 0.5rem;
    padding-left: 1rem;
  }
`

export default TipZone
