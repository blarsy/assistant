import styled from 'styled-components'

export default styled.button`
  text-decoration: none;
  color: #111;
  background-color: #ddd;
  border: 1px solid #111;
  cursor: pointer;
  padding: 0.5rem;
  border-radius: 0.2rem;
  display: inline-block;
  &:hover {
    color: #000;
    background-color: #eee;
  }
  &:focus {
    outline: none;
  }
`
