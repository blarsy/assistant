import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const ClickableContent = styled.span`
  cursor: pointer;
`

class Collapsible extends Component {
  constructor(props) {
    super(props)
    this.props = props
    this.state = {
      collapsed: true
    }
  }

  toggle() {
    this.setState({ collapsed: !this.state.collapsed })
  }

  render() {
    return (
      <div>
        <ClickableContent onClick={() => this.toggle()}>
          {this.props.summary}
        </ClickableContent>
        {!this.state.collapsed && this.props.children}
      </div>
    )
  }
}

Collapsible.propTypes = {
  summary: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
}

export default Collapsible
