import styled from 'styled-components'

const FixedCols = styled.div`
  display: flex;
  > * {
    flex: 1 0 ${props => (100 / +props.cols).toFixed(8)}%;
  }
`

export default FixedCols
