import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Button from './Button'

class GuardedButton extends Component {
  constructor(props) {
    super(props)
    this.props = props
    this.state = {
      armed: false
    }
  }

  toggle() {
    this.setState({ armed: !this.state.armed })
  }

  render() {
    return (
      <div>
        <label htmlFor={`chk${this.props.name}`}>
          <input
            type="checkbox"
            name={`chk${this.props.name}`}
            checked={this.state.armed}
            onChange={() => this.toggle()}
          />
        </label>
        <Button disabled={!this.state.armed} onClick={this.props.action}>{this.props.label}</Button>
      </div>
    )
  }
}

GuardedButton.propTypes = {
  action: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
}

export default GuardedButton
