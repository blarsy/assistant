import React from 'react'
import PropTypes from 'prop-types'
import { map, filter, sort, find } from 'ramda'
import { Collapsible, FixedCols, GuardedButton, TipZone } from './ui'
import agent from './agent/agent'

const PurchasesAdvices = props => {
  const { pairs } = props
  const enrichedPairs = map(entry => {
    const relevantPrice = entry[1].bestBuy
      ? find(entry => entry.btcAmount === 0.3, entry[1].bestBuy).realizedPrice
      : entry[1].price
    return {
      currency: entry[1].currency,
      evolution: relevantPrice / entry[1].lastSalePrice - 1,
      fullInfo: entry[1]
    }
  }, filter(entry => entry[1].lastSalePrice, Object.entries(pairs)))
  const currencies = sort((a, b) => {
    if (a.evolution > b.evolution) {
      return 1
    } else if (a.evolution === b.evolution) {
      return 0
    }
    return -1
  }, filter(pos => !pos.btcAmount, enrichedPairs))

  return (
    <article>
      <h2>Purchase Advices</h2>
      <FixedCols cols="3">
        <span>Currency</span>
        <span>Evolution from last sale</span>
        <span>Actions</span>
      </FixedCols>
      {currencies.map(currency =>
        (<FixedCols cols="3" key={currency.currency}>
          <span>{currency.currency}</span>
          <Collapsible summary={`${(currency.evolution * 100).toFixed(2)} %`}>
            <TipZone>
              <p>
                {currency.fullInfo.bestBuy
                  ? `Buy price for 0.3 BTC: ${find(
                      buy => buy.btcAmount === 0.3,
                      currency.fullInfo.bestBuy
                    ).realizedPrice.toFixed(8)}`
                  : `Market price: ${currency.fullInfo.price.toFixed(8)}`}
              </p>
              {currency.fullInfo.lastSalePrice &&
                <p>Last sale price: {currency.fullInfo.lastSalePrice.toFixed(8)}</p>}
            </TipZone>
          </Collapsible>
          <div>
            <GuardedButton
              label="Buy for 0.3BTC"
              action={() => agent.buyImmediately(currency.fullInfo, 0.3)}
              name={`buy${currency.currency}`}
            />
            {currency.fullInfo.openOrders &&
              currency.fullInfo.openOrders.length > 0 &&
              <span>{currency.fullInfo.openOrders.length} open order(s) would be cancelled</span>}
          </div>
        </FixedCols>)
      )}
    </article>
  )
}

PurchasesAdvices.propTypes = {
  pairs: PropTypes.object.isRequired
}

export default PurchasesAdvices
