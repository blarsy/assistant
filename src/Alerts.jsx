import React from 'react'
import PropTypes from 'prop-types'
import { map } from 'ramda'
import moment from 'moment'
import { GuardedButton, FixedCols } from './ui'

const Alerts = props => {
  const { alerts } = props

  return (
    <article>
      <h2>Alerts</h2>
      {map(
        alert =>
          (<FixedCols cols="2" key={alert.slug}>
            <span>{moment(alert.spawnedAt).format('HH:mm:ss')} - {alert.msg}</span>
            <span>
              {alert.actions &&
                map(
                  action =>
                    (<GuardedButton
                      key={action.name}
                      label={action.name}
                      name={action.name}
                      action={() => action.action()}
                    />),
                  alert.actions
                )}
            </span>
          </FixedCols>),
        alerts
      )}
    </article>
  )
}

Alerts.propTypes = {
  alerts: PropTypes.array.isRequired
}

export default Alerts
