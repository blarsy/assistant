import React from 'react'
import styled from 'styled-components'
import agent from './agent/agent'
import Holdings from './Holdings'
import PurchasesAdvices from './PurchasesAdvices'
import Alerts from './Alerts'

const Dashboard = styled.section`
  display: flex;
  justify-content: space-evenly;
  > * {
    margin: 1rem;
    flex: 1 0;
  }
`
const watchedPairs = [
  'BTC_ETC',
  'BTC_ZEC',
  'BTC_DCR',
  'BTC_DOGE',
  'BTC_BCHSV',
  'BTC_FCT',
  'BTC_BCHABC',
  'BTC_REP',
  'BTC_LTC',
  'BTC_XRP',
  'BTC_STR',
  'BTC_EOS',
  'BTC_XMR',
  'BTC_ETH',
  'BTC_DASH',
  'BTC_XEM',
  'USDT_BTC'
]

export default class App extends React.Component {
  componentDidMount() {
    agent.on('loaded', data => {
      this.setState({ data })
    })
  }
  render() {
    if (this.state && this.state.data) {
      return (
        <Dashboard>
          <Holdings data={this.state.data} />
          <PurchasesAdvices pairs={this.state.data.pairs} />
          <Alerts alerts={this.state.data.alerts} />
        </Dashboard>
      )
    }
    return <div>loading ...</div>
  }
}
